<?php 
declare(strict_types=1);

class Website {
    private array $writers;
    private array $citations;

    public function __construct() {
        $this->writers = [];
        $this->citations = [];
    }

    public function addCitation(Citation $citation): void {
        $this->citations[] = $citation;
    }

    public function ajouterAuteur(Auteur $auteur): void {
        $this->writers[] = $auteur;
    }

    public function getAuteurs(): array {
        return $this->writers;
    }

    public function getCitations(): array {
        return $this->citations;
    }

    public function getAuteurByName(Auteur $auteur): String {
        return $auteur->getFirstName() . " " . $auteur->getLastName();
    }



    public function main(): void {
        // Sample authors
        $author1 = new Auteur("Doe", "John", new DateTime("1980-01-15"));
        $author2 = new Auteur("Smith", "Jane", new DateTime("1975-08-22"));

        // Sample citations
        $citation1 = new Citation("This is the first citation.", $author1, new DateTime("2022-01-01"));
        $citation2 = new Citation("Another citation by John Doe.", $author1, new DateTime("2022-02-10"));
        $citation3 = new Citation("Citation by Jane Smith.", $author2, new DateTime("2022-03-20"));

        // Add authors and citations to the website
        $this->ajouterAuteur($author1);
        $this->ajouterAuteur($author2);

        $this->addCitation($citation1);
        $this->addCitation($citation2);
        $this->addCitation($citation3);
    }
}


$website = new Website();
$website->main();
