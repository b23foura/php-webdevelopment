<?php
declare(strict_types=1);

require_once 'Auteur.php';
require_once 'Citation.php';
require_once 'Website.php';

$website = new Website();
$website->main();

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Toutes les Citations</title>
    <!-- Include other head tags as needed -->
</head>

<body>
    <h1>Toutes les Citations</h1>

    <?php
    // Display all authors and their citations
    foreach ($website->getAuteurs() as $author) {
        echo "<h2>{$author->getFirstName()} {$author->getLastName()}</h2>";
        echo "<p>Naissance: {$author->getBirthDate()->format('Y-m-d')}</p>";

        // Display author's citations
        foreach ($author->getCitations() as $citation) {
            echo "<p>Citation: {$citation->getContent()} (Entrée: {$citation->getEntryDate()->format('Y-m-d')})</p>";
        }
    }
    ?>

</body>

</html>
