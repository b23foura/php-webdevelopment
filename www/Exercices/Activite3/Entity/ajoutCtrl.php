<?php
declare(strict_types=1);

require_once 'Auteur.php';
require_once 'Citation.php';
require_once 'Website.php';

function getCurrentPageURL(): string {
    $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === false ? 'http' : 'https';
    $host = $_SERVER['HTTP_HOST'];
    $script = $_SERVER['SCRIPT_NAME'];
    $params = $_SERVER['QUERY_STRING'];

    $currentURL = $protocol . '://' . $host . $script;
    if (!empty($params)) {
        $currentURL .= '?' . $params;
    }

    return $currentURL;
}

$errorMessage = "";
$login = $citation = $auteur = $date = "";

// Code to retrieve the list of existing authors
$website = new Website();
$existingAuthors = $website->getAuteurs();
$authorList = [];
foreach ($existingAuthors as $existingAuthor) {
    $authorList[] = $existingAuthor->getFirstName() . " " . $existingAuthor->getLastName();
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $login = $_POST["login"];
    $citation = $_POST["citation"];
    $auteur = $_POST["auteur"];
    $date = $_POST["date"];

    // Check if mandatory fields are not empty
    if (empty($login) || empty($citation)) {
        $errorMessage = "Les champs Login et Citation ne peuvent pas être vides.";
    } else {
        // Logic to process the data here (save, validate, etc.)

        // Code to process the author (create a new instance if necessary)
        $authorExists = false;
        foreach ($existingAuthors as $existingAuthor) {
            $fullName = $existingAuthor->getFirstName() . " " . $existingAuthor->getLastName();
            if ($fullName === $auteur) {
                $authorExists = true;
                break;
            }
        }

        if (!$authorExists) {
            // Create a new instance of the author from the input
            $nameParts = explode(" ", $auteur);
            $firstName = $nameParts[0] ?? ''; // Default to an empty string if not set
            $lastName = $nameParts[1] ?? ''; // Default to an empty string if not set

            // Add the birth year if provided
            $birthYear = isset($nameParts[2]) ? intval($nameParts[2]) : null;

            $newAuthor = new Auteur($lastName, $firstName, new DateTime("{$birthYear}-01-01"));
            $website->ajouterAuteur($newAuthor);
        }
    }
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Ajout de Citation</title>
    <style>
        table {
            border: 1px solid #ccccff;
            width: 100%;
        }

        th, td {
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #ccccff;
        }

        .error-message {
            color: red;
        }
    </style>
    <script>
        // JavaScript to handle autocomplete for authors
        var authorList = <?php echo json_encode($authorList); ?>;
        var authorInput = document.getElementsByName("auteur")[0];

        authorInput.addEventListener("input", function () {
            var inputText = this.value.toLowerCase();
            var suggestions = [];

            // Filter authors that match the input
            for (var i = 0; i < authorList.length; i++) {
                if (authorList[i].toLowerCase().includes(inputText)) {
                    suggestions.push(authorList[i]);
                }
            }

            // Display suggestions
            var datalist = document.getElementById("authorSuggestions");
            datalist.innerHTML = "";
            for (var i = 0; i < suggestions.length; i++) {
                var option = document.createElement("option");
                option.value = suggestions[i];
                datalist.appendChild(option);
            }
        });
    </script>
</head>
<body>
<main>
    <article>
        <header><h1>Formulaire de création de citations</h1></header>

        <?php
        if ($_SERVER["REQUEST_METHOD"] === "POST" && empty($errorMessage)) {
            echo "<p>Citation enregistrée avec succès :</p>";
            echo "<p><strong>Login:</strong> $login</p>";
            echo "<p><strong>Citation:</strong> $citation</p>";
            echo "<p><strong>Auteur:</strong> $auteur</p>";
            echo "<p><strong>Date:</strong> $date</p>";
        } else {
            echo "<p class='error-message'>$errorMessage</p>";
        }
        ?>

        <form method="post" name="FrameCitation" action="<?php echo getCurrentPageURL(); ?>">
            <table>
                <tbody>
                <tr>
                    <th><label for="login">Login</label></th>
                    <td><input name="login" maxlength="64" size="32" value="<?php echo htmlspecialchars($login); ?>"></td>
                    <td><?php echo ($errorMessage && empty($login)) ? "Le champ Login ne peut pas être vide." : ""; ?></td>
                </tr>
                <tr>
                    <th><label for="citation">Citation</label></th>
                    <td><textarea cols="128" rows="5" name="citation"><?php echo htmlspecialchars($citation); ?></textarea></td>
                    <td><?php echo ($errorMessage && empty($citation)) ? "Le champ Citation ne peut pas être vide." : ""; ?></td>
                </tr>
                <tr>
                    <th><label for="auteur">Auteur</label></th>
                    <td>
                        <input name="auteur" maxlength="128" size="64" value="<?php echo htmlspecialchars($auteur); ?>"
                               list="authorSuggestions">
                        <datalist id="authorSuggestions"></datalist>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <th><label for="date">Date</label></th>
                    <td><input name="date" type="date" value="<?php echo htmlspecialchars($date ? $date : date('Y-m-d')); ?>"></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <input name="Envoyer" value="Enregistrer la citation" type="submit">
                        <input name="Effacer" value="Annuler" type="reset">
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </article>
</main>
</body>
</html>