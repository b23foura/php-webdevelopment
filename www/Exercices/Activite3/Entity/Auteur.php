<?php
declare(strict_types=1);
class Auteur {
    private String $lastName;
    private String $firstName;
    private DateTime $birthDate;
    private array $citations;

    public function __construct(string $lastName, String $firstName, DateTime $birthDate) {
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->birthDate = $birthDate;
        $this->citations = [];
    }

    public function getLastName(): String {
        return $this->lastName;
    }

    public function getFirstName(): String {
        return $this->firstName;
    }

    public function getBirthDate(): DateTime {
        return $this->birthDate;
    }

    public function getCitations(): array {
        return $this->citations;
    }

    public function addCitation(Citation $citation): void {
        $entryDate = $citation->getEntryDate();
        $birthDate = $this->birthDate;
        $currentDate = new DateTime(); // Current date and time
    
        // Validate entryDate
        if ($entryDate > $birthDate && $entryDate <= $currentDate) {
            $this->citations[] = $citation;
        } else {
            // Handle invalid entryDate (you can throw an exception, log an error, or take other actions)
            echo "Invalid entryDate for citation. Citation not added.";
        }
    }

}