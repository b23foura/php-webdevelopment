<?php 
declare(strict_types=1);

class Citation {
    private String $content;
    private Auteur $writer;
    private DateTime $entryDate;

    public function __construct (String $content, Auteur $auteur, DateTime $entryDate) {
        $this-> content = $content;
        $this->writer = $auteur;
        $this->entryDate = $entryDate;
    }

    public function getContent(): String {
        return $this->content;
    }

    public function getWriter(): Auteur {
        return $this->writer;
    }

    public function getEntryDate(): DateTime {
        return $this->entryDate;
    }
}