<?php
function getCurrentPageURL() {
    $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === false ? 'http' : 'https';
    $host = $_SERVER['HTTP_HOST'];
    $script = $_SERVER['SCRIPT_NAME'];
    $params = $_SERVER['QUERY_STRING'];

    $currentURL = $protocol . '://' . $host . $script;
    if (!empty($params)) {
        $currentURL .= '?' . $params;
    }

    return $currentURL;
}

$errorMessage = "";
$login = $citation = $auteur = $date = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $login = $_POST["login"];
    $citation = $_POST["citation"];
    $auteur = $_POST["auteur"];
    $date = $_POST["date"];

    // Vérifier si les champs obligatoires ne sont pas vides
    if (empty($login) || empty($citation)) {
        $errorMessage = "Les champs Login et Citation ne peuvent pas être vides.";
    } else {
        // Votre logique de traitement des données ici (enregistrement, validation, etc.)
    }
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Ajout de Citation</title>
    <style>
        table {
            border: 1px solid #ccccff;
            width: 100%;
        }

        th, td {
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #ccccff;
        }

        .error-message {
            color: red;
        }
    </style>
</head>
<body>
<main>
    <article>
        <header><h1>Formulaire de création de citations</h1></header>

        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST" && empty($errorMessage)) {
            echo "<p>Citation enregistrée avec succès :</p>";
            echo "<p><strong>Login:</strong> $login</p>";
            echo "<p><strong>Citation:</strong> $citation</p>";
            echo "<p><strong>Auteur:</strong> $auteur</p>";
            echo "<p><strong>Date:</strong> $date</p>";
        } else {
            echo "<p class='error-message'>$errorMessage</p>";
        }
        ?>

        <form method="post" name="FrameCitation" action="<?php echo getCurrentPageURL(); ?>">
            <table>
                <tbody>
                <tr>
                    <th><label for="login">Login</label></th>
                    <td><input name="login" maxlength="64" size="32" value="<?php echo htmlspecialchars($login); ?>"></td>
                    <td><?php echo ($errorMessage && empty($login)) ? "Le champ Login ne peut pas être vide." : ""; ?></td>
                </tr>
                <tr>
                    <th><label for="citation">Citation</label></th>
                    <td><textarea cols="128" rows="5" name="citation"><?php echo htmlspecialchars($citation); ?></textarea></td>
                    <td><?php echo ($errorMessage && empty($citation)) ? "Le champ Citation ne peut pas être vide." : ""; ?></td>
                </tr>
                <tr>
                    <th><label for="auteur">Auteur</label></th>
                    <td><input name="auteur" maxlength="128" size="64" value="<?php echo htmlspecialchars($auteur); ?>"></td>
                    <td></td>
                </tr>
                <tr>
                    <th><label for="date">Date</label></th>
                    <td><input name="date" type="date" value="<?php echo htmlspecialchars($date ? $date : date('Y-m-d')); ?>"></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <input name="Envoyer" value="Enregistrer la citation" type="submit">
                        <input name="Effacer" value="Annuler" type="reset">
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </article>
</main>
</body>
</html>

