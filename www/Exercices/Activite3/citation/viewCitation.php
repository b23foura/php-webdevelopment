<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <title>Vue de la Citation</title>
</head>
<body>
  <main>
    <article>
      <header><h1>Citation enregistrée</h1></header>

      <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
          $login = $_POST["login"];
          $citation = $_POST["citation"];
          $auteur = $_POST["auteur"];
          $date = $_POST["date"];

          echo "<p><strong>Login:</strong> $login</p>";
          echo "<p><strong>Citation:</strong> $citation</p>";
          echo "<p><strong>Auteur:</strong> $auteur</p>";
          echo "<p><strong>Date:</strong> $date</p>";
        } else {
          echo "<p>Erreur lors de la soumission du formulaire.</p>";
        }
      ?>

    </article>
  </main>
</body>
</html>
