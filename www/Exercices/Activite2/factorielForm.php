<!-- factorielForm.php -->
<html>
<head>
    <title>Calcul du factoriel depuis un formulaire</title>
</head>
<body>

<form method="post" action="">
    <label for="number">Entrez un nombre positif :</label>
    <input type="text" name="number" id="number" required>
    <input type="submit" value="Calculer">
</form>

<?php
if(isset($_POST['number']) && is_numeric($_POST['number']) && $_POST['number'] >= 0) {
    $n = $_POST['number'];
    $result = 1;

    for($i = 1; $i <= $n; $i++) {
        $result *= $i;
    }

    echo "Le factoriel de $n est $result.";
} elseif(isset($_POST['number'])) {
    echo "Veuillez fournir un entier positif.";
}
?>

</body>
</html>

