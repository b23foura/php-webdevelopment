<html>
<head>
    <title>Calcul du factoriel depuis l'URL</title>
</head>
<body>

<?php
if(isset($_GET['n']) && is_numeric($_GET['n']) && $_GET['n'] >= 0) {
    $n = $_GET['n'];
    $result = 1;

    for($i = 1; $i <= $n; $i++) {
        $result *= $i;
    }

    echo "Le factoriel de $n est $result.";
} else {
    echo "Veuillez fournir un entier positif dans l'URL.";
}
?>

</body>
</html>
