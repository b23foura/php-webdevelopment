<?php
include "function.php";

echo "<div style='display: flex; flex-wrap: wrap; gap: 4px;'>";

    for ($i = 1; $i <= 10; $i++) {
        echo "<div style='flex: 1 0 18%;'>"; // Flex item with a width of around 18% (adjust as needed)
        tableMult($i, 10);
        echo "</div>";
    }
?>
