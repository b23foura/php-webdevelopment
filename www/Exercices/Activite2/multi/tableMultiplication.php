<?php
include "function.php";

// Retrieve values from the URL parameters
$numero = isset($_GET['numero']) ? intval($_GET['numero']) : 1; // Default to 1 if not provided
$taille = isset($_GET['taille']) ? intval($_GET['taille']) : 10; // Default to 10 if not provided

echo "<div style='display: flex; flex-wrap: wrap; gap: 10px;'>"; // Flex container

echo "<div style='flex: 1 0 100%;'>"; // Flex item taking 100% width for the header
echo "<h2>Table de multiplication de $numero</h2>";
echo "</div>";

// Display the table based on parameters
echo "<div style='flex: 1 0 100%;'>";
tableMult($numero, $taille);
echo "</div>";

echo "</div>";
?>
