<!-- factorielSuite.php -->
<html>
<head>
    <title>Calcul du factoriel de 0 à n depuis l'URL</title>
</head>
<body>

<?php
if(isset($_GET['n']) && is_numeric($_GET['n']) && $_GET['n'] >= 0) {
    $n = $_GET['n'];

    for($i = 0; $i <= $n; $i++) {
        $result = 1;

        for($j = 1; $j <= $i; $j++) {
            $result *= $j;
        }

        echo "Le factoriel de $i est $result.<br>";
    }
} else {
    echo "Veuillez fournir un entier positif dans l'URL.";
}
?>

</body>
</html>

